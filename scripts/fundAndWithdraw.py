from brownie import FundMe
from scripts.helpfulScripts import getAccount
from scripts.deploy import deployFundMe

def fund():
    ctrFundMe = FundMe[-1]
    account = getAccount()
    entranceFee = ctrFundMe.getEntranceFee()
    print(entranceFee)
    print(f"The current entry fee is {entranceFee}")
    print("Funding")
    ctrFundMe.fund({"from": account, "value": entranceFee})

def withdraw():
    ctrFundMe = FundMe[-1]
    account = getAccount()
    ctrFundMe.withdraw({"from": account})


def main():
    fund()
    withdraw()