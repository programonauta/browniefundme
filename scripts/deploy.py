from brownie import FundMe, MockV3Aggregator, network, config
from scripts.helpfulScripts import (
    getAccount,
    deployMocks,
    LOCAL_BLOCKCHAIN_ENVIRONMENTS,
)


def deployFundMe():
    account = getAccount()
    # Pass the price feed address to our fundme contract

    # If we are on a persistent network like Rinkeby, use the associate address
    # otherwise, deploy mocks

    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        priceFeedAddress = config["networks"][network.show_active()]["ethUSD-PriceFeed"]
    else:
        deployMocks()
        priceFeedAddress = MockV3Aggregator[-1].address

    ctrFundMe = FundMe.deploy(
        priceFeedAddress,
        {"from": account},
        publish_source=config["networks"][network.show_active()].get("verify"),
    )
    print(f"Contract deployed to {ctrFundMe.address}")


def main():
    deployFundMe()
