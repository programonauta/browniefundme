
from scripts.helpfulScripts import getAccount, LOCAL_BLOCKCHAIN_ENVIRONMENTS
from scripts.deploy import deployFundMe
from brownie import network, accounts, exceptions
import pytest


def test_canFundAndWithdraw():
    account = getAccount()
    ctrFundMe = deployFundMe()
    entranceFee = ctrFundMe.getEntranceFee() + 100
    tx = ctrFundMe.fund({"from": account, "value": entranceFee})
    tx.wait(1)
    assert ctrFundMe.addressToAmountFunded(account.address) == entranceFee
    tx2 = ctrFundMe.withdraw({"from": account})
    tx2.wait(1)
    assert ctrFundMe.addressToAmountFunded(account.address) == 0


def test_onlyOwnerCanWithdraw():
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip("only for local testing")
    ctrFundMe = deployFundMe()
    bad_actor = accounts.add()
    with pytest.raises(exceptions.VirtualMachineError):
        ctrFundMe.withdraw({"from": bad_actor})